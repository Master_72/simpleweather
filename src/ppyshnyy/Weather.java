package ppyshnyy;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class Weather
{
    private final String token = "1655f919bbcd29ed";
    private String zip;
    private JsonElement json;
    public Weather(String url)
    {
        zip = url;
    }
    public String getLocation()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject()
                .get("full").getAsString();
    }
    public String getTemp()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("temp_f").getAsString();
    }
    public String getWeather()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("weather").getAsString();
    }
    public void fetch()
    {
        String weatherRequest = "http://api.wunderground.com/api/1655f919bbcd29ed/conditions/q/" + zip + ".json";
        try
        {
            URL weatherURL = new URL(weatherRequest);

            InputStream is = weatherURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            JsonParser parser = new JsonParser();
            json = parser.parse(br);
        }
        catch (java.net.MalformedURLException mue)
        {
            System.out.println("URL not valid");
            System.exit(1);
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("IO Exception Caught");
            System.exit(1);
        }
    }
}




