package ppyshnyy;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    TextField zipField;
    @FXML
    TextField cityField;
    @FXML
    TextField tempField;
    @FXML
    TextField weatherField;

    public void handleButtonLaunch(ActionEvent e)
    {
        String input = zipField.getText();
        Weather w = new Weather(input);
        w.fetch();
        weatherField.setText(w.getWeather());
        tempField.setText(w.getTemp());
        cityField.setText(w.getLocation());


    }

}
